function add_to_array(array, object) {
    if(Array.isArray(array))
        array.push(object);

    return array;
}

function insert_into_array(array,index, object) {
    if(Array.isArray(array))
        array.splice(index, 0, object);

    return array;
}

function remove_from_array(array, index) {
    if(Array.isArray(array))
        array.splice(index, 1);
}

function clear_array(array) {
    if(Array.isArray(array)) {
        array.splice(0, array.length);
    }
}
