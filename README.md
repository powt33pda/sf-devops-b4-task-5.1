# Description
This is a test NPM package for Skill Factory DevOps course. It doesn't make much, just a couple of functions for working with arrays.

# Usage
This modules implements 4 basic functions:
 - Append to array
 - Insert into array at certain index
 - Remove from array by index
 - Clear array

